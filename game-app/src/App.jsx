import "./App.css";

import { BrowserRouter, Route, Routes } from "react-router-dom";

import AllGames from "./components/AllGames/AllGames";
import FlappyBird from "./views/FlappyBird/FlappyBird";
import Home from "./views/Home/Home";
import MemoryCards from "./views/MemoryCards/MemoryCards";
import PigGame from "./views/PigGame/PigGame";
import RockPaperScissors from "./views/RockPaperScissors/RockPaperScissors";
import RulesFB from "./components/Rules/RulesFB";
import RulesMC from './components/Rules/RulesMC'
import RulesPG from "./components/Rules/RulesPG";
import RulesRPS from './components/Rules/RulesRPS'
import RulesSD from "./components/Rules/RulesSD";
import RulesTTT from './components/Rules/RulesTTT'
import SideBar from "./views/SideBar/SideBar";
import StreetDice from "./views/StreetDice/StreetDice";
import TicTacToe from "./views/TicTacToe/TicTacToe";

RulesFB
function App() {
  return (
    <BrowserRouter>
      <SideBar>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/all-games" element={<  AllGames />} />        
          <Route path="/tictactoe" element={<TicTacToe />} />
          <Route path="/rules-ttt" element={<RulesTTT />} />
          <Route path="/pig-game" element={<PigGame />} />
          <Route path="/rules-pg" element={<RulesPG />} />
          <Route path="/memorycards" element={<MemoryCards />} />
          <Route path="/rules-mc" element={<RulesMC />} />
          <Route path="/rock-paper-scissors" element={<RockPaperScissors />} />
          <Route path="/rules-rps" element={<RulesRPS />} />
          <Route path="/street-dice" element={<StreetDice />} />
          <Route path="/rules-sd" element={<RulesSD />} />
          <Route path="/flappybird" element={<FlappyBird />} />
          <Route path="/rules-fb" element={<RulesFB />} />
          
        </Routes>
      </SideBar>
    </BrowserRouter>
  );
}

export default App;
{
  /* <div className="w-full h-screen bg-gray-300 object-cover flex items-center">
< SideBar />
</div> */
}
