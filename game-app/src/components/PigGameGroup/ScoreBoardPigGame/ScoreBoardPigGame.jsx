import "./ScoreBoardPigGame.css";

function ScoreBoardPigGame({
  userScore,
  roboScore,
  userTotal,
  roboTotal,
  winsUser,
  winsRobo,
  isUserActive,
}) {
  return (
    <div className="flex flex-col md:flex-row md:gap-6 mt-8">
      <span
        className={`score score-line ${isUserActive === false && "inactive"}`}
      >
        <div className="flex justify-around items-center md:w-[20vw] md:h-[10vh] w-[60vw] bg-gradient-to-r from-[#ffd801] to-[#ffa722]  rounded-lg text-[#F9F9F8] text-lg font-extrabold shadow-xl">
          <div className="md:w-14 md:h-14 w-12 h-12 my-2 rounded-full p-2 shadow-2xl bg-white">
            <img className="" src="/user.jpg" alt="" />
          </div>
          <div className="flex flex-col justify-end items-end">
            <div className="text-sm text-blue-500">WINS: {winsUser}</div>
            <div className="text-sm">SCORE: {userScore}</div>
            <div className="text-sm">TOTAL: {userTotal}</div>
          </div>
        </div>
      </span>
      <span
        className={`score score-line ${isUserActive === true && "inactive"}`}
      >
        <div className="flex justify-around items-center md:w-[20vw] md:h-[10vh] w-[60vw] bg-gradient-to-r  from-[#2afadf] to-[#4c83ff] rounded-lg text-[#F9F9F8] text-lg font-extrabold shadow-xl">
          <div className="md:w-14 md:h-14 w-12 h-12 my-2 rounded-full p-2 shadow-2xl bg-white">
            <img src="/robo.jpg" alt="" />
          </div>
          <div className="flex flex-col justify-end items-end">
            <div className="text-sm text-yellow-600">WINS: {winsRobo}</div>
            <div className="text-sm">SCORE: {roboScore}</div>
            <div className="text-sm">TOTAL: {roboTotal}</div>
          </div>
        </div>
      </span>
    </div>
  );
}

export default ScoreBoardPigGame;
