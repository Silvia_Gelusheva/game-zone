function ScoreBoardMemoryCard({ score, tries }) {
  return (
    <div className=" flex flex-col md:flex-row md:gap-6 gap-3 mt-6 justify-center items-center">
      <div className="flex justify-around items-center md:w-[20vw] md:h-[10vh] w-[60vw] bg-gradient-to-r from-[#ffd801] to-[#ffa722] rounded-lg text-[#F9F9F8] text-lg font-extrabold shadow-xl">
        SCORE: {score}
      </div>
      <div className="flex justify-around items-center  md:w-[20vw] md:h-[10vh] w-[60vw] bg-gradient-to-r  from-[#2afadf] to-[#4c83ff] rounded-lg  text-[#F9F9F8] text-lg font-extrabold shadow-xl">
        TRIES: {tries}
      </div>
    </div>
  );
}

export default ScoreBoardMemoryCard;
