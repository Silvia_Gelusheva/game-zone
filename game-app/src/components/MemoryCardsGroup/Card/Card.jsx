import "./Card.css";

import { useEffect, useState } from "react";

function Card({ card, selectedCards, setSelectedCards }) {
  const [isFlipped, setIsFlipped] = useState(false);

  const handleClick = () => {
    setSelectedCards([...selectedCards, card]);
  };

  useEffect(() => {
    if (selectedCards[0] === card || selectedCards[1] === card || card.match===true) {
      setIsFlipped(true);
    } else {
      setIsFlipped(false);
    }
  },[selectedCards]);

  return (
    <div className={isFlipped ? 'cardM open stop-clicking' : 'cardM'} onClick={handleClick}>
      <div className="front">
        <div className="md:text-5xl text-xl">{card.img}</div>
      </div>
      <div className="back"></div>
    </div>
  );
}

export default Card;
