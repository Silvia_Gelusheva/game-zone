import "./AllGames.css";

import { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";

function AllGames() {
  const navigate = useNavigate();
  const [text, setText] = useState("");

  useEffect(() => {
    setTimeout(() => {
      setText(
        "You can choose between the following games. Read the rules and get fun!"
      );
    }, 1000);
  }, []);

  return (
    <div className="flex flex-row justify-center items-center">
      <div className="flex justify-center md:mt-52 mt-[10px] slide-in-blurred-top mix-blend-multiply">
        <img className=" md:w-[50vh]" src="robo.jpg" alt="" />
      </div>
      <div className="flex flex-col justify-center mt-24 md:ml-8 ml-2">
        <div className="chat chat-start">
          <div className="chat-bubble  bg-[#e7eefb] font-bold text-[#2a2c41] shadow-xl">
            {" "}
            <h1>{text}</h1>
          </div>
        </div>
        <div className="flex flex-col md:grid grid-cols-3 md:gap-6 border-solid border-zinc-500 mt-12">
          <div
            className="flex justify-center md:p-6 p-2 md:text-xl text-sm bg-gradient-to-r from-[#2afadf] to-[#4c83ff] hover:bg-[#e7eefb] font-bold  border-4 border-[#e7eefb]  text-[#2a2c41] hover:text-[#e7eefb]  hover:border-gray-600 shadow-md rounded-xl cursor-pointer"
            onClick={() => navigate("/rules-ttt")}
          >
            Tic Tac Toe
          </div>
          <div
            className="flex justify-center md:p-6 p-2 md:text-xl text-sm bg-gradient-to-r from-[#2afadf] to-[#4c83ff] hover:bg-[#e7eefb] font-bold  border-4 border-[#e7eefb]  text-[#2a2c41] hover:text-[#e7eefb]  hover:border-gray-600 shadow-md rounded-xl cursor-pointer"
            onClick={() => navigate("/rules-rps")}
          >
            Rock Paper Scissors
          </div>
          <div
            className="flex justify-center md:p-6 p-2 md:text-xl text-sm bg-gradient-to-r from-[#2afadf] to-[#4c83ff] hover:bg-[#e7eefb] font-bold  border-4 border-[#e7eefb]  text-[#2a2c41] hover:text-[#e7eefb]  hover:border-gray-600 shadow-md rounded-xl cursor-pointer"
            onClick={() => navigate("/rules-pg")}
          >
            Pig Game
          </div>
          <div
            className="flex justify-center md:p-6 p-2 md:text-xl text-sm bg-gradient-to-r from-[#2afadf] to-[#4c83ff] hover:bg-[#e7eefb] font-bold  border-4 border-[#e7eefb]  text-[#2a2c41] hover:text-[#e7eefb]  hover:border-gray-600 shadow-md rounded-xl cursor-pointer"
            onClick={() => navigate("/rules-sd")}
          >
            Street Dice
          </div>
          <div
            className="flex justify-center md:p-6 p-2 md:text-xl text-sm bg-gradient-to-r from-[#2afadf] to-[#4c83ff] hover:bg-[#e7eefb] font-bold  border-4 border-[#e7eefb]  text-[#2a2c41] hover:text-[#e7eefb]  hover:border-gray-600 shadow-md rounded-xl cursor-pointer"
            onClick={() => navigate("/rules-mc")}
          >
            Memory Cards
          </div>
          <div
            className="flex justify-center md:p-6 p-2 md:text-xl text-sm bg-gradient-to-r from-[#2afadf] to-[#4c83ff] hover:bg-[#e7eefb] font-bold  border-4 border-[#e7eefb]  text-[#2a2c41] hover:text-[#e7eefb]  hover:border-gray-600 shadow-md rounded-xl cursor-pointer"
            onClick={() => navigate("/rules-fb")}
          >
            Flappy Bird
          </div>
        </div>
      </div>
    </div>
  );
}

export default AllGames;
