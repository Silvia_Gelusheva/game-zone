import { FaPlay, FaRegHandScissors, FaStepBackward, FaStepForward } from "react-icons/fa";

import { BsFillBackspaceFill } from "react-icons/bs";
import { useNavigate } from "react-router-dom";

function RulesRPS() {
  const navigate = useNavigate();

  return (
    <div className="flex flex-col justify-center items-center min-h-screen">
      <div className="max-w-sm min-h-[510px] md:bg-gradient-to-r from-[#e7eefb] to-[#e7eefb] md:border-8 md:border-t-4 md:border-gray-400 md:border-b-gray-600 md:shadow-2xl rounded-lg">
        <a href="#" className="flex justify-center">
          <FaRegHandScissors size={100} className="text-[#ffd801] p-4" />
        </a>
        <div className="p-5 ">
          <a href="#">
            <h5 className="mb-4 text-2xl font-bold tracking-tight text-gray-600 flex justify-center underline">
              Rock Paper Scissors
            </h5>
          </a>
          <p className="mb-8 font-normal text-gray-600 text-justify">
            The rock is a closed fist; paper is a flat hand with fingers and
            thumb extended and the palm facing downward; and scissors is a fist
            with the index and middle fingers fully extended toward the opposing
            player. Rock wins against scissors; paper wins against rock; and
            scissors wins against paper.
          </p>
        </div>
      </div>
      <div className="flex flex-row gap-6 mt-5">
        <div
          className="mt-8 bg-[#4c83ff] hover:bg-gradient-to-r hover:from-[#2afadf] hover:to-[#4c83ff] rounded-full shadow-2xl p-6 cursor-pointer"
          onClick={() => navigate("/all-games")}
        >
          <FaStepBackward size={24} className="text-gray-100" />
        </div>

        <div
          className="mt-8 bg-[#ffd801] hover:bg-gradient-to-r hover:from-[#ffd801] hover:to-[#ffa722] rounded-full shadow-2xl  p-6 cursor-pointer"
          onClick={() => navigate("/rock-paper-scissors")}
        >
          <FaPlay size={24} className="text-gray-100" />
        </div>
        <div
          className="mt-8 bg-[#4c83ff] hover:bg-gradient-to-r hover:from-[#2afadf] hover:to-[#4c83ff] rounded-full shadow-2xl p-6 cursor-pointer"
          onClick={() => navigate("/rules-pg")}
        >
          <FaStepForward size={24} className="text-gray-100" />
        </div>
      </div>
    </div>
  );
}

export default RulesRPS;
