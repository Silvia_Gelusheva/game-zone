import { FaPlay, FaStepBackward, FaStepForward } from "react-icons/fa";

import { TbPig } from "react-icons/tb";
import { useNavigate } from "react-router-dom";

function RulesPG() {
  const navigate = useNavigate();

  return (
    <div className="flex flex-col justify-center items-center min-h-screen">
      <div className="flex w-full [h-500px] justify-center items-center">
        <div className="max-w-sm min-h-[510px] md:bg-gradient-to-r from-[#e7eefb] to-[#e7eefb] md:border-8 md:border-t-4 md:border-gray-400 md:border-b-gray-600 md:shadow-2xl rounded-lg">
          <a href="#" className="flex justify-center">
            <TbPig size={100} className="text-[#ffd801] p-4" />
          </a>
          <div className="p-4 ">
            <a href="#">
              <h5 className="mb-4 text-2xl font-bold tracking-tight text-gray-600 flex justify-center underline">
                Pig Game
              </h5>
            </a>
            <p className="mb-8 font-normal text-gray-600 text-justify">
              The winner is the player who gains 30 points in total first.
              Choose a player to go first. That player throws a die and scores
              as many points as the total shown on the die providing the die
              doesn’t roll a 1. The player may continue rolling and accumulating
              points (but risk rolling a 1) or end his turn. If the player rolls
              a 1 his turn is over, he loses all points he accumulated that
              turn, and he passes the die to the next player. Play passes from
              player to player until a winner is determined.
            </p>
          </div>
        </div>
      </div>
      <div className="flex flex-row gap-6 mt-5">
        <div
          className="mt-8 bg-[#4c83ff] hover:bg-gradient-to-r hover:from-[#2afadf] hover:to-[#4c83ff] rounded-full shadow-2xl p-6 cursor-pointer"
          onClick={() => navigate("/rules-rps")}
        >
          <FaStepBackward size={24} className="text-gray-100" />
        </div>

        <div
          className="mt-8 bg-[#ffd801] hover:bg-gradient-to-r hover:from-[#ffd801] hover:to-[#ffa722] rounded-full shadow-2xl  p-6 cursor-pointer"
          onClick={() => navigate("/pig-game")}
        >
          <FaPlay size={24} className="text-gray-100" />
        </div>
        <div
          className="mt-8 bg-[#4c83ff] hover:bg-gradient-to-r hover:from-[#2afadf] hover:to-[#4c83ff] rounded-full shadow-2xl p-6 cursor-pointer"
          onClick={() => navigate("/rules-sd")}
        >
          <FaStepForward size={24} className="text-gray-100" />
        </div>
      </div>
    </div>
  );
}

export default RulesPG;
