import { TfiReload } from "react-icons/tfi";

function GameOver({handleClick}) {
 

  return (
    <div className="flex items-center justify-center mt-4">
      {/* <button
        className=" btn btn-md bg-[#ff724c] shadow-2xl hover:bg-gradient-to-r from-[#ddff3d] to-[#ffc03d] text-[#2a2c41] border-none"
        onClick={handleClick}
      >
        <TfiReload size={32} />
      </button> */}
      <div
            className="bg-gradient-to-r from-blue-500 to-yellow-500 hover:from-[#ffd801] hover:to-[#ffa722]  rounded-full p-4 shadow-2xl cursor-pointer "
            onClick={handleClick}
          >
            <img className="w-10 h-10" src="https://cdn-icons-png.flaticon.com/512/5632/5632370.png" alt="scissors" />
          </div>
    </div>
  );
}

export default GameOver;
