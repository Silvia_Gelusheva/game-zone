import { NavLink } from "react-router-dom";
import { data } from "../../data/data";

function SideBard({ toggle, children }) {
  return (
    <div className="flex w-full min-h-full">
      <div className="sidebar">
        {data.map((d) => {
          return (
            <NavLink
              onClick={() => console.log("game")}
              to={d.path}
              className={`${
                !toggle ? "md:w-[4em] w-[2em]" : "w-[18rem]"
              } flex md:justify-start md:items-start mt-2 py-4 md:px-4 rounded-lg cursor-pointer text-[#F9F9F8] hover:text-[#ffcc4c] md:hover:bg-[#F9F9F8] transition-all duration-300`}
              key={d.id}
            >
              <div className="mr-8 text-[1.7rem] font-semibold">{d.icon}</div>
              <div
                className={`${
                  !toggle ? "opacity-0 delay-200" : ""
                } text-[1rem] font-bold whitespace-pre 
                `}
              >
                {d.text}
              </div>
            </NavLink>
          );
        })}
      </div>
      <main className="w-[100%]">{children}</main>
    </div>
  );
}

export default SideBard;
