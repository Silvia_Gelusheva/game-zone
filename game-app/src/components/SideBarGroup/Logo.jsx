import logo from "/logo.png";

const Logo = ({ toggle }) => {
  return (
    <div
      className={`flex gap-5 items-center ${
        toggle
          ? "bg-none transition-all duration-300 delay-200"
          : "bg-inherit md:p-2"
      }`}
    >
      <div className={`${toggle ? "min-w-[3.5rem] h-[3.5rem]" : "min-w-[3rem] h-[2rem]"} `}>
        <img src={logo} alt="" className={`${toggle ? "w-full h-full object-fit" : "w-[70%] h-[70%] object-cover"} `} />
      </div>
      <div className={!toggle ? "opacity-0 delay-200" : ""}>
        <h3 className="text-xl text-gray-100 font-extrabold uppercase">
          Game Zone
        </h3>
      </div>
    </div>
  );
};

export default Logo;
