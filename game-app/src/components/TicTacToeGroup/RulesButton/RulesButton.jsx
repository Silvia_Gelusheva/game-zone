function RulesButton({handleRulesClick}) {
  return <div className="rules">
       <button
        onClick={handleRulesClick}
        className="bg-white hover:bg-blue-400 text-blue-400 font-bold hover:text-white py-2 px-8 border border-blue-400 hover:border-transparent rounded-lg btn-lg"
      >
        RULES
      </button>
  </div>;
}

export default RulesButton;