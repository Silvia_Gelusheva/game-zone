function ResetButton({ resetBoard }) {
  return <div className="">
      <button 
          className="btn  btn-lg bg-gradient-to-r from-[#ffd801] to-[#ffa722] text-gray-100 hover:bg-gray-600 shadow-2xl border-none mt-8"
          onClick={resetBoard}>RESET</button>
  </div>;
}

export default ResetButton;
