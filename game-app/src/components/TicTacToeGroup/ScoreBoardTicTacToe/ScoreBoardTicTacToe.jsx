import "./ScoreBoardTicTacToe.css";
function ScoreBoardTicTacToe({ scores, playerX }) {
  const { xScore, oScore } = scores;

  return (
    <div className="flex flex-col md:flex-row md:gap-6 md:mt-8 mt-4">
      <span className={`score x-score ${!playerX && "inactive"}`}>
        <div className="flex justify-around items-center md:w-[20vw] md:h-[10vh] w-[60vw] bg-gradient-to-r from-[#ffd801] to-[#ffa722] rounded-lg text-[#F9F9F8] text-lg font-extrabold shadow-xl">
          <div className="w-14 h-14 rounded-full p-2 shadow-2xl bg-white ">
            <img className="" src="/user.jpg" alt="" />
          </div>

          <div className="flex flex-col justify-end items-end">
            <div className="text-lg">X wins - {xScore}</div>
          </div>
        </div>
      </span>
      <span className={`score o-score ${playerX && "inactive"}`}>
        <div className="flex justify-around items-center md:w-[20vw] md:h-[10vh] w-[60vw]  bg-gradient-to-r  from-[#2afadf] to-[#4c83ff] rounded-lg text-[#F9F9F8] text-lg font-extrabold shadow-xl">
          <div className="w-14 h-14 rounded-full p-2 shadow-2xl bg-white ">
            <img src="/robo.jpg" alt="" />
          </div>
          <div className="flex flex-col justify-end items-end">
            <div className="text-sm">
              <div className="text-lg">O wins - {oScore}</div>
            </div>
          </div>
        </div>
      </span>
    </div>
  );
}

export default ScoreBoardTicTacToe;
