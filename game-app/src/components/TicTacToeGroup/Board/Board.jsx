import "./Board.css";

import Box from '../Box/Box'

function Board({ board, onClick }) {
  return (
    <div className="board md:mt-8 mt-2">
      {board.map((value, index) => {
        return <Box key={index} value={value} onClick={() => value=== null && onClick(index)} />;
      })}
    </div>
  );
}

export default Board;
