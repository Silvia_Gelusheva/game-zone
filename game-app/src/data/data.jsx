import { TbCards, TbPig } from "react-icons/tb";

import { AiOutlineHome } from "react-icons/ai";
import { FaEarlybirds } from "react-icons/fa";
import { FaRegHandScissors } from "react-icons/fa";
import { GiRollingDices } from "react-icons/gi";
import { MdGridOff } from "react-icons/md";

export const data = [
  {
    id: 1,
    icon: <AiOutlineHome size={32} className="text-[#ffd801]" />,
    text: "Home",
    path: "/",
  },
  {
    id: 2,
    icon: <MdGridOff size={32} />,
    text: "Tic Tac Toe",
    path: "/tictactoe",
  },
  {
    id: 3,
    icon: <FaRegHandScissors size={32} />,
    text: "Rock Paper Scissors",
    path: "/rock-paper-scissors",
  },
  {
    id: 4,
    icon: <TbPig size={32} />,
    text: "Pig game",
    path: "/pig-game",
  },
  {
    id: 5,
    icon: <GiRollingDices size={32} />,
    text: "Street Dice",
    path: "/street-dice",
  },
  {
    id: 6,
    icon: <TbCards size={32} />,
    text: "Memory Cards",
    path: "/memorycards",
  },
  {
    id: 7,
    icon: <FaEarlybirds size={32} />,
    text: "Flappy Bird",
    path: "/flappybird",
  },
];

export const memoImages = [
  {
    num: 1,
    img: "☕",
    match: false,
  },
  {
    num: 2,
    img: "🔥",
    match: false,
  },
  {
    num: 3,
    img: "🌟",
    match: false,
  },
  {
    num: 4,
    img: "🎁",
    match: false,
  },
  {
    num: 5,
    img: "🚔",
    match: false,
  },
  {
    num: 6,
    img: "🎀",
    match: false,
  },
  {
    num: 7,
    img: "⚽",
    match: false,
  },
  {
    num: 8,
    img: "🎂",
    match: false,
  },
];
