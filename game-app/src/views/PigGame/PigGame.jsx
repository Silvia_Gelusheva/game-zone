// import "./PigGame.css";

import {
  BsDice1,
  BsDice2,
  BsDice3,
  BsDice4,
  BsDice5,
  BsDice6,
} from "react-icons/bs";
import { useEffect, useState } from "react";

import ScoreBoardPigGame from "../../components/PigGameGroup/ScoreBoardPigGame/ScoreBoardPigGame";

function PigGame() {
  const dice = [
    <BsDice1 size={100} />,
    <BsDice2 size={100} />,
    <BsDice3 size={100} />,
    <BsDice4 size={100} />,
    <BsDice5 size={100} />,
    <BsDice6 size={100} />,
  ];
  const [diceRoll, setDiceRoll] = useState(null);
  const [isUserActive, setIsUserActive] = useState(true);

  const [score, setScore] = useState(0);
  const [roboScore, setRoboScore] = useState(0);

  const [totalUser, setTotalUser] = useState(0);
  const [totalRobo, setTotalRobo] = useState(0);

  const [winsUser, setWinsUser] = useState(0);
  const [winsRobo, setWinsRobo] = useState(0);

  const rollDiceUser = () => {
    let num = Math.floor(Math.random() * 6);
    setDiceRoll(dice[num]);

    if (num > 0) {
      setScore(num + 1);
    } else {
      setScore(0);
      setTotalUser(0);
      switchPlayer();
    }
  };

  const rollDiceRobo = () => {
    let num = Math.floor(Math.random() * 6);
    setDiceRoll(dice[num]);
    if (num > 0) {
      setRoboScore(num + 1);
    } else {
      setRoboScore(0);
      setTotalRobo(0);
      switchPlayer();
    }
  };

  const switchPlayer = () => {
    setIsUserActive(!isUserActive);
  };

  const roll = () => {
    if (isUserActive === true) {
      rollDiceUser();
    } else {
      rollDiceRobo();
    }
  };

  useEffect(() => {
    setTotalUser((prev) => prev + score);
  }, [score]);

  useEffect(() => {
    setTotalRobo((prev) => prev + roboScore);
  }, [roboScore]);

  useEffect(() => {
    if (totalUser >= 30) {
      setWinsUser((prev) => prev + 1);
      setScore(0);
      setTotalUser(0);
      setRoboScore(0);
      setTotalRobo(0);
    }
  }, [totalUser]);

  useEffect(() => {
    if (totalRobo >= 30) {
      setWinsRobo((prev) => prev + 1);
      setScore(0);
      setTotalUser(0);
      setRoboScore(0);
      setTotalRobo(0);
    }
  }, [totalRobo]);

  return (
    <div className="flex flex-col justify-center items-center md:min-h-screen">
      <h1 className=" mt-8 font-extrabold text-gray-500 md:text-[2rem] text-xl">
        PIG GAME
      </h1>
      <ScoreBoardPigGame
        isUserActive={isUserActive}
        userScore={score}
        roboScore={roboScore}
        userTotal={totalUser}
        roboTotal={totalRobo}
        winsUser={winsUser}
        winsRobo={winsRobo}
      />
      <div className="flex flex-row md:mt-8 mt-4 justify-center items-center gap-6">
        <div className="flex justify-center items-center md:w-[18vw] md:h-[36vh] w-[50vw] h-[25vh] bg-gradient-to-r from-[#FFFCFF] to-[#D5FEFD] rounded-lg shadow-xl">
          {diceRoll ? (
            <div className="roll-in-blurred-left ">{diceRoll}</div>
          ) : null}
        </div>
      </div>
      <div className="flex flex-row md:mt-2 mt-8 gap-6 justify-center items-center">
        <div
          className="mt-8  bg-gradient-to-r from-blue-500 to-yellow-500 hover:from-[#ffd801] hover:to-[#ffa722]  rounded-full p-4 shadow-2xl cursor-pointer "
          onClick={roll}
        >
          <img
            className="md:w-10 md:h-10 w-8 h-8 mix-blend-multiply "
            src="https://cdn-icons-png.flaticon.com/512/1907/1907763.png"
            alt="dice-roll"
          />
        </div>
        <div
          className="mt-8  bg-gradient-to-r from-blue-500 to-yellow-500 hover:from-[#ffd801] hover:to-[#ffa722]  rounded-full p-4 shadow-2xl cursor-pointer "
          onClick={switchPlayer}
        >
          <img
            className="md:w-10 md:h-10 w-8 h-8 mix-blend-multiply "
            src="https://cdn-icons-png.flaticon.com/512/2911/2911206.png"
            alt="switch-player"
          />
        </div>
      </div>
    </div>
  );
}

export default PigGame;
