import {
  BsDice1,
  BsDice2,
  BsDice3,
  BsDice4,
  BsDice5,
  BsDice6,
  BsFillDice1Fill,
  BsFillDice2Fill,
  BsFillDice3Fill,
  BsFillDice4Fill,
  BsFillDice5Fill,
  BsFillDice6Fill,
} from "react-icons/bs";
import { useEffect, useState } from "react";

import ScoreBoard from "../../components/ScoreBoard/ScoreBoard";

function StreetDice() {
  
  const userDice = [
    <BsDice1 />,
    <BsDice2 />,
    <BsDice3 />,
    <BsDice4 />,
    <BsDice5 />,
    <BsDice6 />,
  ];

  const roboDice = [
    <BsFillDice1Fill />,
    <BsFillDice2Fill />,
    <BsFillDice3Fill />,
    <BsFillDice4Fill />,
    <BsFillDice5Fill />,
    <BsFillDice6Fill />,
  ];

  const [diceUser, setDiceUser] = useState(userDice[0]);
  const [diceUser2, setDiceUser2] = useState(userDice[0]);
  const [diceRobo, setDiceRobo] = useState(roboDice[0]);
  const [diceRobo2, setDiceRobo2] = useState(roboDice[0]);

  const [score, setScore] = useState(0);
  const [roboScore, setRoboScore] = useState(0);

  const [winsUser, setWinsUser] = useState(0);
  const [winsRobo, setWinsRobo] = useState(0);

  const [gamesCount, setGamesCount] = useState(0);

  const rollDice = () => {
    let userNum = Math.floor(Math.random() * 6);
    let roboNum = Math.floor(Math.random() * 6);
    let userNum2 = Math.floor(Math.random() * 6);
    let roboNum2 = Math.floor(Math.random() * 6);

    setDiceUser(userDice[userNum]);
    setDiceUser2(userDice[userNum2]);
    setScore(userNum + 1 + (userNum2 + 1));
    setTimeout(() => {
      setDiceRobo(roboDice[roboNum]);
      setDiceRobo2(roboDice[roboNum2]);
      setRoboScore(roboNum + 1 + (roboNum2 + 1));
      setGamesCount((prev) => prev + 1);
    }, 100);
  };

  useEffect(() => {
    if (score === 7 || score === 10) {
      setWinsUser((prev) => prev + 1);
    }
    if (roboScore === 7 || roboScore === 10) {
      setWinsRobo((prev) => prev + 1);
    }
  }, [score, roboScore]);

  return (
    <div className="flex flex-col justify-center items-center md:min-h-screen">
       <h1 className=" mt-16 font-extrabold text-gray-500 md:text-[2rem] text-xl">
        STREET DICE 7/10
      </h1>
      <ScoreBoard
        userScore={score}
        roboScore={roboScore}
        userTotal={winsUser}
        roboTotal={winsRobo}
        games={"GAMES:"}
        gamesCount={gamesCount}
        counting={"WINS:"}
      />
      <div className="flex flex-row md:mt-8 mt-20 justify-center items-center gap-6">
        <div className="flex justify-center items-center md:w-[20vw] md:h-[40vh] w-[32vw] h-[16vh] bg-gradient-to-r from-[#FFFCFF] to-[#D5FEFD] rounded-lg shadow-xl">
          <div className=" md:text-[5rem] text-[2.5rem]">
            {diceUser}
          </div>
          <div className="mb-8 p-1 md:text-[5.5rem] text-[2.5rem]">
            {" "}
            {diceUser2}
          </div>
        </div>

        <div className="flex justify-center items-center md:w-[20vw] md:h-[40vh] w-[32vw] h-[16vh] bg-gradient-to-r from-[#FFFCFF] to-[#D5FEFD] rounded-lg shadow-xl">
          <div className="md:text-[5rem] text-[2.5rem]">
            {diceRobo}
          </div>
          <div className="mb-8 p-1 md:text-[5.5rem] text-[2.5rem]">
            {" "}
            {diceRobo2}
          </div>
        </div>
      </div>
      <div className="flex flex-row md:mt-8 mt-20 justify-center items-center gap-6">
        <div
          className="mt-8  bg-gradient-to-r from-blue-500 to-yellow-500 hover:from-[#ffd801] hover:to-[#ffa722]  rounded-full p-4 shadow-2xl cursor-pointer "
          onClick={rollDice}
        >
          <img
            className="md:w-10 md:h-10 w-8 h-8"
            src="https://cdn-icons-png.flaticon.com/512/1907/1907763.png"
            alt="dice-roll"
          />
        </div>
      </div>
    </div>
  );
}

export default StreetDice;
