import "./FlappyBird.css";

import { FaEarlybirds } from "react-icons/fa";

function FlappyBird() {
  return (
    <div className="flex flex-col justify-center items-center min-h-screen">   
        <FaEarlybirds className="bounce-in-top text-[#ffc03d]" size={148} />
      <h1 className="text-center mt-8 text-lg">🇨​​​​​🇴​​​​​🇲​​​​​🇮​​​​​🇳​​​​​🇬​​​​​ 🇸​​​​​🇴​​​​​🇴​​​​​🇳​​​​​</h1>
      </div>

  );
}

export default FlappyBird;
