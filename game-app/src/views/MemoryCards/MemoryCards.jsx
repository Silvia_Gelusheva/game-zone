import { useEffect, useState } from "react";

import Card from "../../components/MemoryCardsGroup/Card/Card";
import GameOver from "../../components/GameOver/GameOver";
import ScoreBoardMemoryCard from "../../components/MemoryCardsGroup/ScoreBoardMemoryCard/ScoreBoardMemoryCard";
import { memoImages } from "../../data/data";
import { useNavigate } from "react-router-dom";

function MemoryCards() {
  const [cards, setCards] = useState([]);
  const [selectedCards, setSelectedCards] = useState([]);
  const [score, setScore] = useState(0);
  const [tries, setTries] = useState(0);
  const [gameOver, setGameOver] = useState(false);
  const navigate = useNavigate();

  const shuffleImages = () => {
    let arr = [...memoImages, ...memoImages]
      .map((item, i) => ({
        ...item,
        id: i + 1,
      }))
      .sort((a, b) => 0.5 - Math.random());
    setScore(0);

    setCards(arr);
  };

  const checkMatch = () => {
    if (selectedCards[0].num === selectedCards[1].num) {
      setScore((prev) => prev + 1);
      let updatedCards = cards.map((card) => {
        if (card.num === selectedCards[0].num) {
          return { ...card, match: true };
        }
        return card;
      });

      setCards(updatedCards);
    } else {
      setTries((prev) => prev + 1);
    }
  };

  useEffect(() => {
    shuffleImages();
  }, []);

  useEffect(() => {
    console.log(selectedCards);
    if (selectedCards.length === 2) {
      setSelectedCards([]);
      checkMatch();
    }
  }, [selectedCards]);

  useEffect(() => {
    if (score === memoImages.length) {
      setTimeout(() => {
        shuffleImages();
        setGameOver(true);
      }, 1000);
    }
  }, [score, shuffleImages]);

  const handleRulesMCClick = () => {
    navigate("/rules-mc");
  };
  function newGame() {
    window.location.reload(false);
  }
  return (
    <div className=" flex flex-col min-h-screen">
        <h1 className=" flex justify-center mt-14 font-extrabold text-gray-500 md:text-[2rem] text-xl">
       MEMORY CARDS
      </h1>
     <div className="md:mt-0 mt-16">
     <ScoreBoardMemoryCard score={score} tries={tries} />
     </div>

      {gameOver && <GameOver handleClick={newGame} />}

      <div className=" flex justify-center item-center md:mt-8 mt-16">
        <div className="grid grid-cols-4 gap-4">
          {cards.map((card) => {
            return (
              <Card
                key={card.id}
                card={card}
                selectedCards={selectedCards}
                setSelectedCards={setSelectedCards}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default MemoryCards;
