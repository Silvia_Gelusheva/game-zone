import Board from "../../components/TicTacToeGroup/Board/Board";
import ResetButton from "../../components/TicTacToeGroup/ResetButton/ResetButton";
import RulesButton from "../../components/TicTacToeGroup/RulesButton/RulesButton";
import ScoreBoardTicTacToe from "../../components/TicTacToeGroup/ScoreBoardTicTacToe/ScoreBoardTicTacToe";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
function TicTacToe() {
  const [board, setBoard] = useState(Array(9).fill(null));
  const [playerX, setPlayerX] = useState(true);
  const [scores, setScores] = useState({ xScore: 0, oScore: 0 });
  const [gameOver, setGameOver] = useState(false);
  const navigate = useNavigate();
  const WINS = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  const handleClick = (idx) => {
    const updatedBoard = board.map((value, index) => {
      if (index === idx) {
        return playerX === true ? "x" : "o";
      } else {
        return value;
      }
    });

    const checkWinner = (board) => {
      for (let i = 0; i < WINS.length; i++) {
        const [x, y, z] = WINS[i];

        if (board[x] && board[x] === board[y] && board[y] === board[z]) {
          setGameOver(true);
          return board[x];
        }
      }
    };

    const winner = checkWinner(updatedBoard);

    if (winner) {
      if (winner === "o") {
        let { oScore } = scores;
        oScore += 1;
        setScores({ ...scores, oScore });
      } else {
        let { xScore } = scores;
        xScore += 1;
        setScores({ ...scores, xScore });
      }
    }
    setBoard(updatedBoard);
    setPlayerX(!playerX);
  };

  const resetBoard = () => {
    setGameOver(false);
    setBoard(Array(9).fill(null));
  };

  const handleRulesClick = () => {
    navigate("/rules-ttt");
  };

  return (
    <div className="flex flex-col justify-center items-center md:min-h-screen">
    <h1 className=" mt-8 font-extrabold text-gray-500 md:text-[2rem] text-xl">
        TIC-TAC-TOE
      </h1>
      {/* <RulesButton handleRulesClick={handleRulesClick} /> */}
      <ScoreBoardTicTacToe scores={scores} playerX={playerX} />
      <Board board={board} onClick={gameOver ? resetBoard : handleClick} />
      <div
            className="mt-8 bg-gradient-to-r from-blue-500 to-yellow-500 hover:from-[#ffd801] hover:to-[#ffa722]  rounded-full p-4 shadow-2xl cursor-pointer "
            onClick={() => resetBoard()}
          >
            <img className="md:w-10 md:h-10 w-8 h-8" src="https://cdn-icons-png.flaticon.com/512/5632/5632370.png" alt="scissors" />
          </div>
    </div>
  );
}

export default TicTacToe;
