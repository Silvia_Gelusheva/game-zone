import { useEffect, useState } from "react";

import GameOver from "../../components/GameOver/GameOver";
import ScoreBoard from "../../components/ScoreBoard/ScoreBoard";

function RockPaperScissors() {
  const items = ["rock", "paper", "scissors"];

  const [userItem, setUserItem] = useState(null);
  const [roboItem, setRoboItem] = useState(null);
  const [score, setScore] = useState(0);
  const [roboScore, setRoboScore] = useState(0);
  const [winsUser, setWinsUser] = useState(0);
  const [winsRobo, setWinsRobo] = useState(0);

  const [final, setFinal] = useState(null);
  const [gameOver, setGameOver] = useState(false);

  const handleUserChoice = (value) => {
    setUserItem(value);
    handleRoboChoice();
  };

  const handleRoboChoice = () => {
    const randomItem = items[Math.floor(Math.random() * items.length)];
    setTimeout(() => {
      setRoboItem(randomItem);
    }, 100);
  };

  const handleResult = () => {
    switch (userItem + roboItem) {
      case "scissorspaper":
      case "rockscissors":
      case "paperrock":
        setScore((prev) => prev + 1);
        break;
      case "paperscissors":
      case "scissorsrock":
      case "rockpaper":
        setRoboScore((prev) => prev + 1);
        break;
      case "paperpaper":
      case "scissorsscissors":
      case "rockrock":
        break;
    }
  };

  useEffect(() => {
    if (score === 3) {
      setFinal("You win!");
      setWinsUser((prev) => prev + 1);
      setGameOver(true);
    } else if (roboScore === 3) {
      setFinal("Robo wins!");
      setWinsRobo((prev) => prev + 1);
      setGameOver(true);
    }
  }, [score, roboScore]);

  useEffect(() => {
    handleResult();
  }, [userItem, roboItem]);

  function newGame() {
    setScore(0);
    setRoboScore(0);
    setGameOver(false);
    setFinal(null);
  }

  return (
    <div className="flex flex-col justify-center items-center md:min-h-screen">
       <h1 className=" mt-16 font-extrabold text-gray-500 md:text-[2rem] text-xl">
        ROCK-PAPER-SCISSORS
      </h1>
      <ScoreBoard
        userScore={score}
        roboScore={roboScore}
        counting={"WINS:"}
        userTotal={winsUser}
        roboTotal={winsRobo}
      />

      <div className="flex flex-row md:mt-8 mt-20 justify-center items-center gap-6">
        <div className="flex justify-center items-center md:w-[20vw] md:h-[40vh] w-[32vw] h-[16vh] bg-gradient-to-r from-[#FFFCFF] to-[#D5FEFD] rounded-lg shadow-xl">
          {userItem === "rock" && (
            <img className="w-20 h-20" src="/rock.png" alt="rock" />
          )}
          {userItem === "paper" && (
            <img className="w-20 h-20" src="/paper.png" alt="paper" />
          )}
          {userItem === "scissors" && (
            <img className="w-20 h-20" src="/scissors.png" alt="scissors" />
          )}
        </div>

        <div className="flex justify-center items-center  md:w-[20vw] md:h-[40vh] w-[32vw] h-[16vh] bg-gradient-to-r from-[#FFFCFF] to-[#D5FEFD] rounded-lg shadow-xl">
          {roboItem === "rock" && (
            <img className="w-20 h-20" src="/rock.png" alt="rock" />
          )}
          {roboItem === "paper" && (
            <img className="w-20 h-20" src="/paper.png" alt="paper" />
          )}
          {roboItem === "scissors" && (
            <img className="w-20 h-20" src="/scissors.png" alt="scissors" />
          )}
        </div>
      </div>

      {gameOver ? (
        <GameOver handleClick={newGame} />
      ) : (
        <div className="flex flex-row gap-4 md:mt-2 mt-20 justify-center items-center">
          <div
            className="mt-8 bg-gradient-to-r from-[#ffd801] to-[#ffa722] hover:from-[#FFFCFF] hover:to-[#D5FEFD] rounded-full p-4 shadow-2xl cursor-pointer "
            onClick={() => handleUserChoice(items[0])}
          >
            <img className="md:w-10 md:h-10 w-8 h-8" src="/rock.png" alt="rock" />
          </div>
          <div
            className="mt-8 bg-gradient-to-r from-gray-400 to-gray-600 hover:from-[#FFFCFF] hover:to-[#D5FEFD] rounded-full p-4 shadow-2xl cursor-pointer "
            onClick={() => handleUserChoice(items[1])}
          >
            <img className="md:w-10 md:h-10 w-8 h-8" src="/paper.png" alt="paper" />
          </div>
          <div
            className="mt-8 bg-gradient-to-r from-[#2afadf] to-[#4c83ff] hover:from-[#FFFCFF] hover:to-[#D5FEFD] rounded-full p-4 shadow-2xl cursor-pointer "
            onClick={() => handleUserChoice(items[2])}
          >
            <img className="md:w-10 md:h-10 w-8 h-8" src="/scissors.png" alt="scissors" />
          </div>
        </div>
      )}
      <p className="flex justify-center items-center mt-4 text-[#ff724c] text-2xl font-bold uppercase animate-pulse">
        {final}
      </p>
    </div>
  );
}

export default RockPaperScissors;
