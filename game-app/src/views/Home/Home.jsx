import "./Home.css";

import AllGames from "../../components/AllGames/AllGames";
import { useNavigate } from "react-router-dom";

function Home() {
  // const [toggle, setToggle] = useState(false);
  const navigate = useNavigate();

  return (
    <div className="flex flex-row justify-center items-center">
      <div className="flex justify-center md:mt-52 mt-[300px] roll-in-left  mix-blend-multiply">
        <img className=" w-[50vh]" src="/robo.jpg" alt="" />
      </div>
      {/* {!toggle && ( */}
      <div className="bounce-in-top md:mt-0 mt-36">
        <div className="chat chat-start ">
          <div className="chat-bubble  bg-[#e7eefb] font-bold text-[#2a2c41] shadow-xl">
            <h1>Hey, I'm Robo! Welcome to Game Zone! Enjoy your time!</h1>
          </div>
        </div>
        <div className="flex justify-center mt-8">
          <button
            className="btn bg-gradient-to-r from-[#ffd801] to-[#ffa722]  text-[#e7eefb] animate-pulse hover:animate-none border-none "
            onClick={() => navigate("/all-games")}
          >
            Learn more...
          </button>
        </div>
      </div>
      {/* )} */}
      {/* {toggle && <AllGames />} */}
    </div>
  );
}

export default Home;
