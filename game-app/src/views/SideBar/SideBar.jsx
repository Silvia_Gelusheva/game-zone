import { BiChevronLeft } from "react-icons/bi";
import Logo from '../../components/SideBarGroup/Logo'
import SideBarData from '../../components/SideBarGroup/SideBarData'
import { useState } from "react";

function SideBar({ children }) {
  const [toggle, setToggle] = useState(false);

  return (
    <div className="flex">
      <div
        className={`${
          toggle
            ? "bg-gradient-to-r from-[#2afadf] to-[#4c83ff] h-[96%] w-[20rem] lg:ml-6 md:ml-6 p-4 transition-all border-solid border-glass relative"
            : "bg-gradient-to-r from-[#2afadf] to-[#4c83ff] h-[96%] md:w-[5.8rem] w-[4rem] lg:ml-6 md:ml-6 p-4 transition-all border-solid border-glass relative"
        } min-h-screen`}
      >
        <Logo toggle={toggle} />
        <div className="divider"></div> 
        <SideBarData toggle={toggle} />
        <div
          className="absolute md:top-[7rem] top-[6rem] flex justify-center items-center lg:-left-5 md:lg:-left-5 w-10 h-10 bg-gradient-to-r from-[#2afadf] to-[#4c83ff] rounded-full cursor-pointer"
          onClick={() => setToggle(!toggle)}
        >
          <BiChevronLeft
            className={`${
              !toggle ? "rotate-180" : ""
            } text-3xl text-[#F9F9F8] transition-all duration-300`}
          />
        </div>
      </div>
      <main>{children}</main>
    </div>
  );
}

export default SideBar;
