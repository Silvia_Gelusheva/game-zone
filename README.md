# Game Zone

This app combines some simple, but very popular games:

1. Tic Tac Toe
2. Rock-Paper-Scissors
3. Pig Game
4. Street Dice
5. Memory Cards

### Technologies:

- JavaScript
- React.js
- CSS
- Tailwind CSS


### Setup

Open the root directory game-zone folder. Install all packages by running: *`npm install`*

Then run: *`npm run dev`*


#### **Home page**

<img src='Game Zone Pics/Home.png' width='600px'>

<br/>

#### **All Games**

<img src='Game Zone Pics/all-games.png' width='600px'>

<br/> 

#### **Rules**

<img src='Game Zone Pics/Rules.png' width='600px'>

<br/>


#### **Tic Tac Toe**

<img src='Game Zone Pics/tic-tac-toe.png' width='600px'>

<br/>

#### **Rock Paper Scissors**

<img src='Game Zone Pics/rock-paper-scissors.png' width='600px'>

<br/>

#### **Pig Game**

<img src='Game Zone Pics/pig-game.png' width='600px'>

<br/>

#### **Street Dice**

<img src='Game Zone Pics/street-dice.png' width='600px'>

<br/>

#### **Memory Cards**

<img src='Game Zone Pics/memory-cards.png' width='600px'>

<br/>
